from ql.api import http
from urllib.parse import urlparse
import time
import datetime
from ql.api.authentication import sign_request

"""
Quicklizrd API Client module.

Provides a class used to perform GET / POST / PUT operations against Quicklizard's REST API
"""
class Client(object):
  
    API_HOST = 'https://rest.quicklizard.com'
  
    def __init__(self, key, secret, host = None):
        """
        Create a new instance of Quicklizard's API client

        :param key: API authentication key
        :param secret: API authentication secret
        :param baseURL: API base URL
        """
        self.key = key
        self.secret = secret
        if host is None:
            self.host = self.API_HOST
        else:
            self.host = host

    def get(self, endpoint):
        """
        Perform a GET HTTP request to endpoint and return response object

        :param endpoint: API call endpoint. For example '/api/v2/products?page=1&per_page=50'
        :return: HTTP Response
        """
        url = self.get_uri(endpoint)
        endpoint = self.get_api_endpoint(url.path, url.query)
        digest = sign_request(self.secret, url.path, qs=url.query)
        return http.get(self.host, endpoint, self.key, digest)

    def post(self, endpoint, payload):
        """
        Perform a POST HTTP request to endpoint with payload and return response object

        :param endpoint: API call endpoint. For example '/api/v2/products/bulk_create'
        :param payload: {"products": [ {"client_uid": "abc", "price": 12.3} ] }
        :return: HTTP Response
        """
        url = self.get_uri(endpoint)
        endpoint = self.get_api_endpoint(url.path, url.query)
        digest = sign_request(self.secret, url.path, qs=url.query, body=payload)
        return http.post(self.host, endpoint, payload, self.key, digest)

    def put(self, endpoint, payload):
        """
        Perform a PUT HTTP request to endpoint with payload and return response object

        :param endpoint: API call endpoint. For example '/api/v2/products/bulk_update'
        :param payload: {"products": [ {"client_uid": "abc", "price": 12.3} ] }
        :return: HTTP Response
        """
        url = self.get_uri(endpoint)
        endpoint = self.get_api_endpoint(url.path, url.query)
        digest = sign_request(self.secret, url.path, qs=url.query, body=payload)
        return http.put(self.host, endpoint, payload, self.key, digest)

    def upload(self, endpoint, file):
        """
        Perform a PUT HTTP request to endpoint with payload and return response object

        :param endpoint: API call endpoint. For example '/api/v2/products/bulk_update'
        :param file: path to file to upload. For example '/tmp/bulk/products.txt'
        :return: HTTP Response
        """
        url = self.get_uri(endpoint)
        endpoint = self.get_api_endpoint(url.path, url.query)
        digest = sign_request(self.secret, url.path, qs=url.query)
        return http.upload(self.host, endpoint, file, self.key, digest)

    def get_uri(self, endpoint):
        """
        Take API endpoint, add timestamp query-string parameter and return a parsed URL object with API host and full endpoint with query-string

        :param endpoint: API call endpoint. For example '/api/v2/products?page=1&per_page=50'
        :return: parsed URL
        """
        timestamp = int(time.mktime(datetime.datetime.now().timetuple()))
        if '?' in endpoint:
            path = '{}&qts={}'.format(endpoint, timestamp)
        else:
            path = '{}?qts={}'.format(endpoint, timestamp)
        full_url = '{}{}'.format(self.host, path)
        return urlparse(full_url)

    def get_api_endpoint(self, path, qs):
        """
        Takes request path and query-string, joins them into a single path and returns them

        :param path: request path
        :param qs: request query-string
        :return: joined request path and query-string
        """
        return '{}?{}'.format(path, qs)
