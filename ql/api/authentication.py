import json
import hashlib


def sign_request(api_secret, path, qs=None, body=None):
    """Join path, query-string (optional), request-body (optional) and API secret.
    Return their SHA256 hash which is used as an API request signature

    :param api_secret: Quicklizard API secret
    :param path: API request path
    :param qs: API request query-string
    :param body: API request body
    :return: API request signature
    """
    parts = [path]
    if qs is not None:
        parts.append(qs)
    if body is not None:
        if type(body) is dict:
            parts.append(json.dumps(body))
        else:
            parts.append(body)
    parts.append(api_secret)
    digest = hashlib.sha256(''.join(parts).encode("utf-8")).hexdigest()
    return digest
