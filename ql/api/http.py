import requests


def get(host, endpoint, key, digest):
    """
    Performs HTTP GET request and returns response

    :param host: request host
    :param endpoint: request path with query-string
    :param key: API authentication key
    :param digest: API authentication secret
    :return: HTTP response
    """
    request_url = '{}{}'.format(host, endpoint)
    headers = get_request_headers(key, digest)
    req = requests.Request("GET", request_url, data="", headers=headers)
    return get_response(req)


def post(host, endpoint, payload, key, digest):
    """
    Performs HTTP POST request and returns response

    :param host: request host
    :param endpoint: request path with query-string
    :param payload: request body
    :param key: API authentication key
    :param digest: API authentication secret
    :return: HTTP response
    """
    request_url = '{}{}'.format(host, endpoint)
    headers = get_request_headers(key, digest)
    if type(payload) is dict:
        req = requests.Request("POST", request_url, json=payload, headers=headers)
    else:
        req = requests.Request("POST", request_url, data=payload, headers=headers)
    return get_response(req)


def upload(host, endpoint, file, key, digest):
    """
    Performs HTTP POST request and returns response

    :param host: request host
    :param endpoint: request path with query-string
    :param file: path to file to upload
    :param key: API authentication key
    :param digest: API authentication secret
    :return: HTTP response
    """
    request_url = '{}{}'.format(host, endpoint)
    headers = get_request_headers(key, digest)
    files = {'upload': open(file, 'rb')}
    req = requests.Request("POST", request_url, files=files, headers=headers)
    return get_response(req)


def put(host, endpoint, payload, key, digest):
    """
    Performs HTTP PUT request and returns response

    :param host: request host
    :param endpoint: request path with query-string
    :param payload: request body
    :param key: API authentication key
    :param digest: API authentication secret
    :return: HTTP response
    """
    request_url = '{}{}'.format(host, endpoint)
    headers = get_request_headers(key, digest)
    if type(payload) is dict:
        req = requests.Request("PUT", request_url, json=payload, headers=headers)
    else:
        req = requests.Request("PUT", request_url, data=payload, headers=headers)
    return get_response(req)


def get_request_headers(key, digest):
    """
    Returns HTTP request headers dictionary
    :param key: API authentication key
    :param digest: API authentication secret
    :return: HTTP request headers dict.
    """
    return {
      'API_KEY': key,
      'API_DIGEST': digest
    }


def get_response(req):
    """
    Takes an HTTP request object, performs the request and returns the response
    :param req: HTTP request
    :return: HTTP response
    """
    prepared = req.prepare()
    session = requests.Session()
    res = session.send(prepared, timeout=60, verify=False)
    session.close()
    return res