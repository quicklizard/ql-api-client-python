# Quicklizard API Client - Python

Authenticate and perform operations against Quicklizard's REST API from your Python application.

## Installation

Add this line to your application's requirements.txt:

#### Python 3
```text
# ql-api-client-python
-e git+https://quicklizard@bitbucket.org/quicklizard/ql-api-client-python.git#egg=master
```

#### Python 2.7
```text
# ql-api-client-python
-e git+https://quicklizard@bitbucket.org/quicklizard/ql-api-client-python.git#egg=py27-master
```

And then execute:

    $ pip install -r requirements.txt

## Usage

```python
from ql.api import Client
api_key = 'YOUR_API_KEY' #replace with your API key
api_secret = 'YOUR_API_SECRET' #replace with your API secret
client = Client(api_key, api_secret)
```

### GET Requests

```python
import json
from ql.api import Client
api_key = 'YOUR_API_KEY' #replace with your API key
api_secret = 'YOUR_API_SECRET' #replace with your API secret
client = Client(api_key, api_secret)
response = client.get('/api/v2/recommendations/all?per_page=1')
data = json.loads(response.text)
print data
```
---
### POST Requests

```python
import json
from ql.api import Client
api_key = 'YOUR_API_KEY' #replace with your API key
api_secret = 'YOUR_API_SECRET' #replace with your API secret
client = Client(api_key, api_secret)
payload = {
  'products': [ {'product_id': 'abc', ...} ] #see API docs for complete structure
}
response = client.post('/api/v2/products/create', payload)
data = json.loads(response.text)
print data
```
---
### PUT Requests

```python
import json
from ql.api import Client
api_key = 'YOUR_API_KEY' #replace with your API key
api_secret = 'YOUR_API_SECRET' #replace with your API secret
client = Client(api_key, api_secret)
payload = {
  'products': [ {'product_id': 'abc', ...} ] #see API docs for complete structure
}
response = client.put('/api/v2/products/update', payload)
data = json.loads(response.text)
print data
```
---

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-api-client-python.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

