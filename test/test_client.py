import os, sys, unittest, json

cwd = os.path.dirname(os.path.realpath(__file__))
sys.path.append('{}/..'.format(cwd))

api_key = ''
api_secret = ''

from ql.api import Client


class TestClient(unittest.TestCase):
  
    def setUp(self):
        self.client = Client(api_key, api_secret)

    def test_get_success(self):
        response = self.client.get('/api/v2/recommendations/all?client_key=dummy')
        self.assertEqual(response.status_code, 200)

    def test_get_response(self):
        response = self.client.get('/api/v2/recommendations/all?client_key=dummy&per_page=1')
        data = json.loads(response.text)
        num_of_products = len(data['result'])
        self.assertEqual(num_of_products, 1)

    def test_post_success(self):
        payload = {
          'payload': [ {'product_id': 'aa-bbb-ccc', 'client_key': 'dummy'} ]
        }
        response = self.client.post('/api/v2/products/create', payload)
        self.assertEqual(response.status_code, 200)

    def test_post_failure(self):
        payload = {
          'prod': []
        }
        response = self.client.post('/api/v2/products/create', payload)
        self.assertEqual(response.status_code, 401)

    def test_put_success(self):
        payload = {
          'payload': [ {'product_id': 'aa-bbb-ccc', 'client_key': 'dummy'} ]
        }
        response = self.client.put('/api/v2/products/update', payload)
        self.assertEqual(response.status_code, 200)

    def test_put_failure(self):
        payload = {
          'prod': []
        }
        response = self.client.put('/api/v2/products/update', payload)
        self.assertEqual(response.status_code, 401)


if __name__ == '__main__':
    unittest.main()
