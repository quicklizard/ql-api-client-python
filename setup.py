import uuid
from setuptools import setup
import os
try: # for pip >= 10
  from pip._internal.req import parse_requirements
except ImportError: # for pip <= 9.0.3
  from pip.req import parse_requirements

def read(fname):
  return open(os.path.join(os.path.dirname(__file__), fname)).read()

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
reqs_file = os.path.join(BASE_DIR, 'requirements.txt')
install_reqs = parse_requirements(reqs_file, session=uuid.uuid1())

setup(
  name="ql-api-client",
  version="0.0.1",
  author="Zohar Arad",
  author_email="zohar@quicklizard.com",
  description=("Quicklizard REST API Client for Python"),
  license="MIT",
  keywords="rest, api",
  install_requires=["requests"],
  url="https://bitbucket.org/quicklizard/ql-api-client-python",
  packages=[],
  include_package_data=False,
  long_description=read('README.md'),
  scripts=[],
  classifiers=[
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python :: 2.7"
  ],
)